<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
  <?php 
	$usuarioLogado = selecionarUsuarioPorId($usuarioId); 
  ?>
  
  <div class="navbar-nav ml-auto">
	   Seja bem-vindo, <?php echo $usuarioLogado['nome'] ?>
	   <br>
	   <div>
	   <a href="logout.php">Sair</a>
	   <div>
  </div>

</nav>