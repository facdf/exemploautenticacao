<?php

	function selecionarUsuarioPorId($id){
		$conexao = conexao();
		$consulta = " SELECT * FROM tb_usuario WHERE id = " . $id;
		$resultado = mysqli_query($conexao, $consulta);
		$dados = mysqli_fetch_assoc($resultado);
		return $dados;
	}
	
	function selecionarTodosUsuarios(){
		$conexao = conexao();
		$consulta = " SELECT usuario.*, perfil.id as perfil_id,
		                     perfil.nome as nome_perfil 
		              FROM tb_usuario as usuario 
						LEFT JOIN tb_perfil as perfil ON 
						usuario.perfil_id = perfil.id
					  ORDER BY nome ";
		$resultado = mysqli_query($conexao, $consulta);
		return $resultado;
	}
	
?>