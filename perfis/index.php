<?php

require '../config_inicial.php';
require '../conexao.php';
require '../autenticado.php';

//UTILIZADO PARA MOSTRAR A TABELA COM OS REGISTROS DO BANCO DE DADOS
$resultado = selecionarTodosUsuarios();

// UTILIZADO PARA A FUNCIONALIDADE DE EDIÇÃO
$dadosEdicao = null;
if(isset($_REQUEST['id'])) {
	$id = $_REQUEST['id'];
	$dadosEdicao = selecionarUsuarioPorId($id);
}
 
?>
	
<!DOCTYPE html>
<html lang="en">

	<?php require '../head.php' ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require '../menu_lateral.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		<?php require '../menu_superior.php'; ?>
        <!-- End of Topbar -->

        <div class="row">
         <div class="col-lg-12">
            <div class="p-5">
              <form class="user" method="get" action="<?php echo NOME_SISTEMA ?>/usuarios/inserir.php">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" placeholder="Nome" name="nome" value="<?php echo $dadosEdicao['nome']?>">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" placeholder="E-mail" name="email" value="<?php echo $dadosEdicao['email']?>">
                  </div>
                </div>
				<div class="form-group row">
				  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" placeholder="Senha" name="senha" value="<?php echo $dadosEdicao['senha']?>">
                  </div>
				</div>
				
				<input type="hidden" name="id" value="<?php echo $dadosEdicao['id']?>">
                <input type="submit" class="btn btn-primary btn-user btn-block col-sm-3" value="Gravar">
                  
                </input>
              </form>

            </div>
          </div>
        </div>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Lista de Alunos</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Email</th>
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Nome</th>
                      <th>Email</th>
                      <th>Ações</th>
                    </tr>
                  </tfoot>
                  <tbody>
					<?php
						while($linha = mysqli_fetch_array($resultado)) {
					?>
						<tr>
							<td><?php echo $linha["id"] ?> </td>
							<td><?php echo $linha["nome"] ?></td>
							<td><?php echo $linha["email"] ?></td>
							<td>
								<a href="index.php?id=<?php echo $linha["id"] ?>">Editar</a>
								<a href="deletar.php?id=<?php echo $linha["id"] ?>">Excluir</a>
							</td>
						</tr>
					<?php
						}
					?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
	  <?php require '../rodape.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
	<?php require '../footer.php' ?>
</body>
</html>
