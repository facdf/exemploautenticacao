<?php

	function selecionarTodos(){
		$conexao = conexao();
		$consulta = " SELECT *, aluno.id as id_aluno, 
		                        endereco.id as id_endereco 
		              FROM tb_aluno as aluno 
						LEFT JOIN tb_endereco as endereco ON
						aluno.id = endereco.aluno_id
					  WHERE status = 0
					  ORDER by nome ";
		$resultado = mysqli_query($conexao, $consulta);
		return $resultado;
	}
	
	function selecionarPorId($id){
		$conexao = conexao();
		$consulta = " SELECT *, aluno.id as id_aluno, 
		                        endereco.id as id_endereco 
		              FROM tb_aluno as aluno,
						   tb_endereco as endereco
					  WHERE aluno.id = endereco.aluno_id 
					     and aluno.id = " . $id;
		$resultado = mysqli_query($conexao, $consulta);
		$dados = mysqli_fetch_assoc($resultado);
		return $dados;
	}
	
	function contarRegistros() {
		$conexao = conexao();
		$consulta = " SELECT * FROM tb_aluno order by nome ";
		$resultado = mysqli_query($conexao, $consulta);
		$dados = mysqli_fetch_all($resultado);
		return count($dados);
	}
?>